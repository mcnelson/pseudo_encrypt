Gem::Specification.new do |s|
  s.name        = 'pseudo_encrypt'
  s.version     = '0.0.1'
  s.date        = '2019-02-26'
  s.summary     = "Ruby implementation of Daniel Vérité's Postgresql function pseudo_encrypt."
  s.description =
    "WIP"
  s.authors     = ["Michael Nelson"]
  s.email       = 'michael@nelsonware.com'
  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- test/*`.split("\n")
  s.homepage    = 'https://gitlab.com/mcnelson/pseudo_encrypt'
  s.license     = 'MIT'

  # s.require_paths = ["lib"]
  s.required_ruby_version = '>= 2.5.1'

  s.add_development_dependency "minitest", "~> 5.11.3"
end
