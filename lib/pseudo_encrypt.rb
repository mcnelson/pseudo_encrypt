class PseudoEncrypt
  ALPHABET = ('A'..'Z').to_a + ('0'..'9').to_a.freeze
  UNCONFUSED_ALPHABET = ALPHABET.reject { |chr| chr == 'O' || chr == '0' }.freeze
  attr_reader :output_length, :unconfuse_o

  def initialize(output_length, unconfuse_o: true)
    @output_length = output_length
    @unconfuse_o = unconfuse_o
  end

  def formula32(x)
    ((((1366 * x + 150889) % 714025) / 714025.0) * 32768)
  end

  def formula24(x)
    ((((1366 * x + 150889) % 714025) / 714025.0) * (4096 - 1))
  end

  def encrypt32(n)
    l1 = (n >> 16) & 65535
    l2 = 0
    r1 = n & 65535
    r2 = 0

    3.times do
      l2 = r1
      r2 = l1 ^ formula32(r1).to_i
      l1 = l2
      r1 = r2
    end

    ((r1 << 16) + l1)
  end

  def encrypt24(n)
    l1 = (n >> 12) & (4096 - 1)
    l2 = 0
    r1 = n & (4096 - 1)
    r2 = 0

    3.times do
      l2 = r1
      r2 = l1 ^ formula24(r1).to_i
      l1 = l2
      r1 = r2
    end

    ((l1 << 12) + r1)
  end

  def constrained_encrypt24(n)
    begin
      n = encrypt24(n)
    end while n >= (ending_int_with_given_length)

    starting_int_with_given_length + n
  end

  def constrained_encrypt32(n)
    begin
      n = encrypt32(n)
    end while n >= (ending_int_with_given_length)

    starting_int_with_given_length + n
  end

  def unconstrained_encode24(n)
    encode(encrypt24(n))
  end

  def unconstrained_encode32(n)
    encode(encrypt32(n))
  end

  def encode24(n)
    encode(constrained_encrypt24(n))
  end

  def encode32(n)
    encode(constrained_encrypt32(n))
  end

  def encode(n)
    length = alphabet.length
    output = ''
    remaining = n

    while remaining.positive?
      output << alphabet[remaining % length]
      remaining /= length
    end

    output
  end

  private

  def starting_int_with_given_length
    alphabet.length ** (output_length - 1)
  end

  def ending_int_with_given_length
    alphabet.length ** output_length
  end

  def alphabet
    @alphabet ||= unconfuse_o ? UNCONFUSED_ALPHABET : ALPHABET
  end
end
