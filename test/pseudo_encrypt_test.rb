require_relative '../lib/pseudo_encrypt'
require 'minitest/autorun'

class PseudoEncryptTest < Minitest::Test
  def assert_entropy(ints, first_n_digits, threshold = 5)
    groups = ints.group_by do |i|
      i / (10 ** (Math.log10(i).floor + 1 - first_n_digits))
    end

    groups = groups.
             map { |prefix, collection| [prefix, collection.count] }.
             select { |_, count| count > threshold }.
             sort_by(&:last).
             reverse

    assert_empty groups
  end

  def test_32bit_7chr_entropy
    pseudo_encrypt = PseudoEncrypt.new(7)

    assert_entropy 1000.times.map { |n| pseudo_encrypt.constrained_encrypt32(n) },
                   4,
                   4
  end

  def test_32bit_6chr_entropy
    pseudo_encrypt = PseudoEncrypt.new(6)

    assert_entropy 1000.times.map { |n| pseudo_encrypt.constrained_encrypt32(n) },
                   4,
                   5
  end

  def test_24bit_6chr_entropy
    pseudo_encrypt = PseudoEncrypt.new(6)

    assert_entropy 1000.times.map { |n| pseudo_encrypt.constrained_encrypt24(n) },
                   4,
                   3
  end
end
